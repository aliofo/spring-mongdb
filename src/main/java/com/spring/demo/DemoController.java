package com.spring.demo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.spring.demo.model.AddressBook;
import com.spring.demo.model.User;
import com.spring.demo.service.UserService;

@Controller
public class DemoController extends BaseController{
	@Resource
	private UserService userService;
	/**
	 * jsp view test
	 */
	@RequestMapping("/demo")
	public String demoTest(){
		return "demo";
	}
	/**
	 * json to string test
	 */
	@RequestMapping("/json")
	public @ResponseBody String jsonTest(){
		return "dddd";
	}
	/**
	 * json object test
	 */
	@RequestMapping("/jsonObject")
	public @ResponseBody Integer jsonObjectTest(){
		return new Integer(5);
	}
	@RequestMapping("/user")
	public void save(){
		User user = new User();
		user.setName("张三");
		userService.save(user);
	}
	
	@RequestMapping("/addressBook")
	@ResponseBody
	public String saveAddressBook(){
		AddressBook addressBook = new AddressBook();
		addressBook.setUserId("123123");
		addressBook.setUserName("王安全");
		addressBook.setAddressBook("梁桂英:+8615106506545");
		addressBook.setCreateDate(new Date());
		userService.saveAddressBook(addressBook);
		return "成功";
	}
	
	@RequestMapping("/getAddressBook")
	@ResponseBody
	public String getAddressBook() throws JsonProcessingException{
		AddressBook addressBook = new AddressBook();
		addressBook.setUserId("123123");
		addressBook.setUserName("王安全");
		//addressBook.setAddressBook("梁桂英:+8615106506545");
		//addressBook.setCreateDate(new Date());
		AddressBook ab = userService.getAddressBook(addressBook);
		
		return returnJsonStr(ab);
	}
	
	@RequestMapping("/getAddressBookAll")
	@ResponseBody
	public String getAddressBookAll() throws JsonProcessingException{
		AddressBook addressBook = new AddressBook();
		addressBook.setUserId("123123");
		addressBook.setUserName("王安全");
		//addressBook.setAddressBook("梁桂英:+8615106506545");
		//addressBook.setCreateDate(new Date());
		List<AddressBook> list = userService.getAddressBookAll(addressBook);
		return returnJsonStr(list);
	}
	
	@RequestMapping("/insertList")
	@ResponseBody
	public String insertList() {
		ArrayList<Object> arrayList = new ArrayList<>();
		
		for (int i = 0; i < 100; i++) {
			AddressBook addressBook = new AddressBook();
			addressBook.setUserId("000123456");
			addressBook.setUserName("wangaq");
			addressBook.setAddressBook("郑强:+8615165589913" + i);
			addressBook.setCreateDate(new Date());
			
			arrayList.add(addressBook);
		}
		userService.insertList(arrayList);
		return "";
	}
	
	@RequestMapping("/getPageList")
	@ResponseBody
	public String getPageList(int skip, int limit) {
		AddressBook addressBook = new AddressBook();
		addressBook.setUserId("000123456");
		//addressBook.setUserName("wangaq");
		//addressBook.setAddressBook("郑强:+8615165589913" + i);
		//addressBook.setCreateDate(new Date());
		List<AddressBook> list = userService.getPageList(addressBook,skip,limit);
		return returnJsonStr(list);
	}

}
