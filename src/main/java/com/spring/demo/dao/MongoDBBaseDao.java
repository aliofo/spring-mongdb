package com.spring.demo.dao;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository("mongoDBBaseDao")
public class MongoDBBaseDao implements BaseDao {
	
	@Resource
	protected MongoTemplate mongoTemplate;
	
	@Override
	public <T> T findById(Class<T> entityClass, String id) {
		
		return mongoTemplate.findById(id, entityClass);
	}

	@Override
	public <T> List<T> findAll(Class<T> entityClass) {
		return mongoTemplate.findAll(entityClass);
	}

	@Override
	public void remove(Object record) {
		mongoTemplate.remove(record);

	}

	@Override
	public void add(Object record) {
		
		mongoTemplate.insert(record); 

	}

	@Override
	public void saveOrUpdate(Object record) {
		
		mongoTemplate.save(record);
	}

	@Override
	public <T> T findOne(Class<T> entityClass) {
		return mongoTemplate.findOne(new Query(), entityClass);
	}
	
	@Override
	public void batchToSave(List<Object> list, Class<?> entityClass) {
		mongoTemplate.insert(list, entityClass);
	}
	
	@Override
	public void batchToSave1(List<Object> list) {
		mongoTemplate.insertAll(list);
	}
	
	@Override
	public <T> List<T> findPageList(Class<T> entityClass, Query query, int skip, int limit) {
		query.skip(skip);
		query.limit(limit);//返回的数量
		return mongoTemplate.find(query, entityClass);
	}

	@Override
	public <T> long count(Query query, Class<T> entityClass) {
		return mongoTemplate.count(query, entityClass);
	}
	
	/*private Query getQuery(User criteriaUser) {
		if (criteriaUser == null) {
			criteriaUser = new User();
		}
		Query query = new Query();
		if (criteriaUser.getId() != null) {
			Criteria criteria = Criteria.where("id").is(criteriaUser.getId());
			query.addCriteria(criteria);
		}
		if (criteriaUser.getAge() > 0) {
			Criteria criteria = Criteria.where("age").gt(criteriaUser.getAge());
			query.addCriteria(criteria);
		}
		if (criteriaUser.getName() != null) {
			Criteria criteria = Criteria.where("name").regex("^" + criteriaUser.getName());
			query.addCriteria(criteria);
		}
		return query;
	}
	
	public long count(User criteriaUser) {
		Query query = getQuery(criteriaUser);
		return mongoTemplate.count(query, User.class);
	}*/


}
