package com.spring.demo.dao;

import java.util.List;

import org.springframework.data.mongodb.core.query.Query;

import com.spring.demo.model.AddressBook;

public interface BaseDao {
	
	<T> T findById(Class<T> entityClass, String id);
	
	<T> T findOne(Class<T> entityClass);
	
	<T> List<T> findAll(Class<T> entityClass);
	
	void remove(Object record);
	
	void add(Object record);
	
	void saveOrUpdate(Object record);

	/**
	 * @param list
	 * @param entityClass
	 */
	void batchToSave(List<Object> list, Class<?> entityClass);

	/**
	 * @param list
	 */
	void batchToSave1(List<Object> list);

	/**
	 * @param entityClass
	 * @param query
	 * @param skip
	 * @param limit
	 * @return
	 */
	<T> List<T>findPageList(Class<T> entityClass, Query query, int skip, int limit);

	/**
	 * @param <T>
	 * @param query
	 * @param class1
	 * @return
	 */
	<T> long count(Query query, Class<T> entityClass);

}
