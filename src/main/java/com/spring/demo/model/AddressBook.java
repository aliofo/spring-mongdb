/**
 * 
 */
package com.spring.demo.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @Description:
 * @author wangaq
 * @date 2018年8月7日
 *
 */

@Document
public class AddressBook implements Serializable{

	private static final long serialVersionUID = -5290309663642897163L;

	@Id
	private String id;

	private String userId;
	
	private String userName;

	private String addressBook;

	@CreatedDate
	private Date createDate;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the addressBook
	 */
	public String getAddressBook() {
		return addressBook;
	}

	/**
	 * @param addressBook the addressBook to set
	 */
	public void setAddressBook(String addressBook) {
		this.addressBook = addressBook;
	}

	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
