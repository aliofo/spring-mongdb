package com.spring.demo.service;

import java.util.ArrayList;
import java.util.List;

import com.spring.demo.model.AddressBook;
import com.spring.demo.model.User;

public interface UserService {
	
	public void save(User user);

	/**
	 * @param addressBook
	 */
	public void saveAddressBook(AddressBook addressBook);

	/**
	 * @param addressBook
	 */
	public AddressBook getAddressBook(AddressBook addressBook);

	/**
	 * @param addressBook
	 * @return
	 */
	public List<AddressBook> getAddressBookAll(AddressBook addressBook);

	/**
	 * @param arrayList
	 */
	void insertList(ArrayList<Object> arrayList);

	/**
	 * @param addressBook
	 * @param skip
	 * @param limit
	 * @return
	 */
	List<AddressBook> getPageList(AddressBook addressBook, int skip, int limit);

}
