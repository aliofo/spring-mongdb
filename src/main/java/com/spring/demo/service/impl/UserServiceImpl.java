
package com.spring.demo.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.spring.demo.dao.BaseDao;
import com.spring.demo.model.AddressBook;
import com.spring.demo.model.User;
import com.spring.demo.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	@Resource
	private BaseDao baseDao;

	@Override
	public void save(User user) {
		baseDao.saveOrUpdate(user);
	}

	@Override
	public void saveAddressBook(AddressBook addressBook) {
		baseDao.saveOrUpdate(addressBook);
	}

	@Override
	public AddressBook getAddressBook(AddressBook addressBook) {
		return baseDao.findOne(addressBook.getClass());
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AddressBook> getAddressBookAll(AddressBook addressBook) {
		return (List<AddressBook>) baseDao.findAll(addressBook.getClass());
	}

	@Override
	public void insertList(ArrayList<Object> arrayList) {
		baseDao.batchToSave1(arrayList);
	}
	
	/**
	 *分页可实现在实体父类里
	 */
	@Override
	public List<AddressBook> getPageList(AddressBook addressBook,int skip, int limit) {
		Query query = getQuery(addressBook);
		return (List<AddressBook>) baseDao.findPageList(addressBook.getClass(), query, skip, limit);
	}

	private Query getQuery(AddressBook addressBook) {
		if (addressBook == null) {
			addressBook = new AddressBook();
		}
		Query query = new Query();
		if (addressBook.getId() != null) {
			Criteria criteria = Criteria.where("id").is(addressBook.getId());
			query.addCriteria(criteria);
		}
		if (addressBook.getUserId() != null) {
			Criteria criteria = Criteria.where("userId").is(addressBook.getUserId());
			query.addCriteria(criteria);
		}
		return query;
	}

	public long count(AddressBook addressBook) {
		Query query = getQuery(addressBook);
		return baseDao.count(query, AddressBook.class);
	}

}
